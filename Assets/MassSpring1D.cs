﻿using UnityEngine;
using System.Collections;

public class MassSpring1D : MonoBehaviour {

	private GameObject[] arrOfRigidbodies;
	public int amountOfJoints = 5;
	public float ropeLength = 10;
	public Material ropeMaterial;
	private Rigidbody theChosenOne;
	private float slRatio = 0;
	private float positionX = 0;
	public GameObject node;
	public bool enableBendingSprings = true;
	public float springVal = 0;
	public float springDamp = 0;
	public float totalWeight = 0.3f;

	void Awake()
	{	
		slRatio = ropeLength/amountOfJoints;
		gameObject.name = "Rope: " + enableBendingSprings.ToString();
		arrOfRigidbodies = new GameObject[amountOfJoints];
		positionX = transform.position.x;


		for(int i = 0; i < amountOfJoints; i++)
		{
			arrOfRigidbodies[i] = CreateNewNode();

			if(i == amountOfJoints - 1)
				theChosenOne = arrOfRigidbodies[i].rigidbody;
		}

		for(int i = 0; i < arrOfRigidbodies.Length; i++)
		{
			if(i < arrOfRigidbodies.Length - 1)
			{
				SetLineRenderer(arrOfRigidbodies[i]);

				if(i + 1 < arrOfRigidbodies.Length)
					AddStructuralSpring(arrOfRigidbodies[i], arrOfRigidbodies[i+1]);

				if(i + 2 < arrOfRigidbodies.Length && enableBendingSprings)
					AddBendingSpring(arrOfRigidbodies[i], arrOfRigidbodies[i + 2]);
			}
		}
	}

	GameObject CreateNewNode()
	{
		GameObject source = (GameObject)Instantiate(node, new Vector3(positionX, transform.position.y , transform.position.z), Quaternion.identity);
		source.transform.parent = transform;
		if(source.GetComponent<Rigidbody>() == null)
			source.AddComponent<Rigidbody>();
		source.rigidbody.mass = totalWeight / amountOfJoints;
		source.rigidbody.drag = 0;
		source.rigidbody.angularDrag = 0;

		SphereCollider col = source.AddComponent<SphereCollider>();
		col.radius = 2f;
		
		positionX += slRatio;
		return source;
	}

	void SetLineRenderer(GameObject source)
	{
		LineRenderer lineRenderer = source.AddComponent<LineRenderer>();
		lineRenderer.SetWidth(1f, 1f);
		lineRenderer.material = ropeMaterial;
	}

	void AddStructuralSpring(GameObject source, GameObject to)
	{
		SpringJoint structureSpring = source.AddComponent<SpringJoint>();
		structureSpring.spring = springVal;
		structureSpring.maxDistance = 0.0f;
		structureSpring.damper = springDamp;
		structureSpring.connectedBody = to.rigidbody;
	}

	void AddBendingSpring(GameObject source, GameObject to)
	{
		SpringJoint bendingSpring = source.AddComponent<SpringJoint>();	
		bendingSpring.spring = springVal;
		bendingSpring.maxDistance = 0.0f;
		bendingSpring.damper = springDamp;
		bendingSpring.connectedBody = to.rigidbody;
	}

	void FixedUpdate()
	{
		theChosenOne.rigidbody.AddForce(Input.GetAxis("Vertical") * 1000 * Vector3.forward, ForceMode.Acceleration);
		theChosenOne.rigidbody.AddForce(Input.GetAxis("Horizontal") * 1000 * Vector3.right, ForceMode.Acceleration);

		for(int i = 0; i < arrOfRigidbodies.Length - 1; i++)
		{	LineRenderer line = arrOfRigidbodies[i].GetComponent<LineRenderer>();
			line.SetPosition(0, arrOfRigidbodies[i].transform.position);
			line.SetPosition(1, arrOfRigidbodies[i + 1].transform.position);
		}
	}
}
