﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MassSpring3D : MonoBehaviour
{
	public int gridSize = 3;
	public float pointSize = 1f;
	public float pointDistance = 2f;
	public float springVal = 100f;
	public float springDamp = 1f;
	public float totalWeight = 1f;
	public Material ropeMaterial;
	public bool structureSprings = false;
	public bool bendingSprings = false;
	public bool shearSprings = false;
	public bool renderPoints = false;

	public int numberOfSprings = 0;
	private GameObject pointPrefab;
	private GameObject[ , , ] points ; // 3 dimensional array for holding points

	void Start ()
	{
		// Instantiate all objects
		if (pointPrefab == null)
			pointPrefab = GameObject.CreatePrimitive (PrimitiveType.Cube);
		pointPrefab.AddComponent<Rigidbody> ();
		pointPrefab.GetComponent<Rigidbody>().mass = totalWeight / (gridSize * gridSize * gridSize);
		//pointPrefab.GetComponent<Rigidbody>().useGravity = false;

		InstantiateWobblyObject ();

		Destroy (pointPrefab);
	}

	void InstantiateWobblyObject ()
	{
		points = new GameObject[gridSize, gridSize, gridSize];
		for (int i = 0; i < gridSize; i++)
		{
			for (int j = 0; j < gridSize; j++)
			{
				for (int k = 0; k < gridSize; k++)
				{
					Vector3 position = new Vector3 (i * pointDistance, j * pointDistance, k * pointDistance) + transform.position;
					string name = "( " + i.ToString() + " , " + j.ToString() + " , " + k.ToString() + " )" ;
					points [i, j, k] = CreatePoint (position, name);

				}
			}
		}

		Transform[,] frontPlane = new Transform[points.GetLength(0), points.GetLength(1)];
		Transform[,] rightPlane = new Transform[points.GetLength(2), points.GetLength(1)];
		Transform[,] backPlane = new Transform[points.GetLength(0), points.GetLength(1)];
		Transform[,] leftPlane = new Transform[points.GetLength(2), points.GetLength(1)];
		Transform[,] bottomPlane = new Transform[points.GetLength(0), points.GetLength(2)];
		Transform[,] topPlane = new Transform[points.GetLength(0), points.GetLength(2)];

		for (int i = 0; i < gridSize; i++)
		{
			for (int j = 0; j < gridSize; j++)
			{
				for (int k = 0; k < gridSize; k++)
				{
					#region Adding Springs
					GameObject source = points[i,j,k];
					if (structureSprings)
					{
						bool show = false;
						// Left to right
						if(i < points.GetLength(0) - 1)
							AddSpring(source, points[i + 1, j, k], show);
						// Bottom to up
						if(j < points.GetLength(1) - 1)
							AddSpring(source, points[i, j + 1, k], show);
						// Front to back
						if(k < points.GetLength(2) - 1)
							AddSpring(source, points[i, j, k + 1], show);
					}

					if (bendingSprings)
					{
						bool show = false;
						// Left to right
						if(i < points.GetLength(0) - 2)
							AddSpring(source, points[i + 2, j, k], show);
						// Bottom to up
						if(j < points.GetLength(1) - 2)
							AddSpring(source, points[i, j + 2, k], show);
						// Front to back
						if(k < points.GetLength(2) - 2)
							AddSpring(source, points[i, j, k + 2], show);
					}

					if(shearSprings)
					{
						bool show = false;
						// Right and up
						if(i < points.GetLength(0) - 1 && j < points.GetLength(1) - 1)
							AddSpring(source, points[i + 1, j + 1, k], show);
						// Left and up
						if(i > 0 && j < points.GetLength(1) - 1)
							AddSpring(source, points[i - 1, j + 1, k], show);
						// Forward and up
						if(j < points.GetLength(1) - 1 && k < points.GetLength(2) - 1)
							AddSpring(source, points[i, j + 1, k + 1], show);
						// Backwards and up
						if(k > 0 && j < points.GetLength(1) - 1)
							AddSpring(source, points[i, j + 1, k - 1], show);

						// right and forward
						if(i < points.GetLength(0) - 1 && k < points.GetLength(2) - 1)
							AddSpring(source, points[i + 1, j, k + 1], show);
						// left and forward
						if(i > 0 && k < points.GetLength(2) - 1)
							AddSpring(source, points[i - 1, j, k + 1], show);

						// forward, right and up (diagonal)
						if(i < points.GetLength(0) - 1 && j < points.GetLength(1) - 1 && k < points.GetLength(2) - 1)
							AddSpring(source, points[i + 1, j + 1, k + 1], show);
						// forward, left and up (diagonal)
						if(i > 0 && j < points.GetLength(1) - 1 && k < points.GetLength(2) - 1)
							AddSpring(source, points[i - 1, j + 1, k + 1], show);
						// forward, right and down (diagonal)
						if(i < points.GetLength(0) - 1 && j > 0 && k < points.GetLength(2) - 1)
							AddSpring(source, points[i + 1, j - 1, k + 1], show);
						// forward, left and down (diagonal)
						if(i > 0 && j > 0 && k < points.GetLength(2) - 1)
							AddSpring(source, points[i - 1, j - 1, k + 1], show);

					}
					#endregion

					// Add mesh
					// Verts positions
					// 0 ------ 1
					// |      /	|
					// |    /	|
					// |  /    	|
					// 2 ------ 3
					if(k == 0)
						frontPlane[i,j] = points[i,j,k].transform;
					if(k == points.GetLength(2) - 1)
						backPlane[j,i] = points[i,j,k].transform;
					
					if(i == 0)
						leftPlane[j,k] = points[i,j,k].transform;
					if(i == points.GetLength(0) - 1)
						rightPlane[k,j] = points[i,j,k].transform;

					if(j == 0)
						bottomPlane[k,i] = points[i,j,k].transform;
					if(j == points.GetLength(1) - 1)
						topPlane[i,k] = points[i,j,k].transform;

				}
			}
		}

		GameObject frontPlaneObj = new GameObject("FrontPlane");
		frontPlaneObj.AddComponent<PlaneMesh>().CreateMesh(frontPlane);
		frontPlaneObj.GetComponent<PlaneMesh>().mat = ropeMaterial;
		frontPlaneObj.transform.parent = this.transform;

		GameObject leftPlaneObj = new GameObject("LeftPlane");
		leftPlaneObj.AddComponent<PlaneMesh>().CreateMesh(leftPlane);
		leftPlaneObj.GetComponent<PlaneMesh>().mat = ropeMaterial;
		leftPlaneObj.transform.parent = this.transform;

		GameObject rightPlaneObj = new GameObject("RightPlane");
		rightPlaneObj.AddComponent<PlaneMesh>().CreateMesh(rightPlane);
		rightPlaneObj.GetComponent<PlaneMesh>().mat = ropeMaterial;
		rightPlaneObj.transform.parent = this.transform;

		GameObject backPlaneObj = new GameObject("BackPlane");
		backPlaneObj.AddComponent<PlaneMesh>().CreateMesh(backPlane);
		backPlaneObj.GetComponent<PlaneMesh>().mat = ropeMaterial;
		backPlaneObj.transform.parent = this.transform;

		GameObject bottomPlaneObj = new GameObject("BottomPlane");
		bottomPlaneObj.AddComponent<PlaneMesh>().CreateMesh(bottomPlane);
		bottomPlaneObj.GetComponent<PlaneMesh>().mat = ropeMaterial;
		bottomPlaneObj.transform.parent = this.transform;

		GameObject topPlaneObj = new GameObject("TopPlane");
		topPlaneObj.AddComponent<PlaneMesh>().CreateMesh(topPlane);
		topPlaneObj.GetComponent<PlaneMesh>().mat = ropeMaterial;
		topPlaneObj.transform.parent = this.transform;
	}


	GameObject CreatePoint (Vector3 pos, string name)
	{
		GameObject go = GameObject.Instantiate (pointPrefab, pos, Quaternion.identity) as GameObject;
		//Destroy(go.GetComponent<Collider>());
		go.transform.localScale = new Vector3 (pointSize, pointSize, pointSize);
		go.transform.parent = transform;
		go.name = name;
		go.tag = "point";
		go.renderer.material = ropeMaterial;
		if(!renderPoints)
			go.renderer.enabled = false;
		return go;
	}

	void AddSpring (GameObject source, GameObject to, bool drawLine)
	{
		SpringJoint structureSpring = source.AddComponent<SpringJoint> ();
		structureSpring.spring = springVal;
		structureSpring.maxDistance = 0.0f;
		structureSpring.damper = springDamp;
		structureSpring.connectedBody = to.rigidbody;

		//source.AddComponent<DragRigidBody> ();
		numberOfSprings++;
		// Add line renderer
		if (drawLine)
		{
			SpringDrawer drawer = source.AddComponent<SpringDrawer> ();
			drawer.target = to.transform;
			drawer.width = 0.1f;
			drawer.mat = ropeMaterial;
			drawer.Init ();
		}
	}

	// Draw the area that the blob will cover on initialization
	void OnDrawGizmos ()
	{
		if (!Application.isPlaying)
		{
			Gizmos.color = new Color (0.2f, 0, 0, 0.5F);
			float posAddition = (((float)gridSize - 1) / 2f) * pointDistance;
			Vector3 center = transform.position + new Vector3 (posAddition, posAddition, posAddition);
			Vector3 size = new Vector3 ((gridSize - 1) * pointDistance, (gridSize - 1) * pointDistance, (gridSize - 1) * pointDistance);
			Gizmos.DrawCube (center, size);
		}
	}
}
//250888-1382