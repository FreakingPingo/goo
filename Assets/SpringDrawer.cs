﻿using UnityEngine;
using System.Collections;

public class SpringDrawer : MonoBehaviour {
	public Transform target;
	public float width = 0.2f;
	public Material mat;
	private LineRenderer line;
	// Use this for initialization
	public void Init () 
	{
		GameObject go = new GameObject("line to " + target.name);
		go.transform.parent = this.transform;
		line = go.AddComponent<LineRenderer>();
		line.material = mat;
		line.SetWidth(width, width);
		line.SetColors(Color.red, Color.blue);
	}

	void UpdateLine()
	{
		line.SetPosition(0, transform.position);
		line.SetPosition(1, target.transform.position);
	}
	
	// Update is called once per frame
	void Update () {
		UpdateLine();
	}
}
