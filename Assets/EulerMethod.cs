﻿using UnityEngine;
using System.Collections;

public class EulerMethod : MonoBehaviour {
	public float velocity = 5f;
	float mass = 1f;
	float springConstant =30f;
	public bool newMethod;

	float min, max;
	
	// Update is called once per frame
	void FixedUpdate () {
		float acceleration = 1/mass * (-springConstant * transform.position.x);
		float newvelocity = velocity + acceleration * Time.fixedDeltaTime;
		float newPosition = transform.position.x + velocity * Time.fixedDeltaTime;

		if(newMethod)
		{
			float newAcceleration = 1/mass * (-springConstant * newPosition);
			transform.position = new Vector3(transform.position.x + ((velocity + newvelocity) / 2) * Time.fixedDeltaTime,transform.position.y,0);
			velocity = velocity + ((0.5f * acceleration) + (0.5f * newAcceleration)) * Time.fixedDeltaTime;
		}
		else
		{
			transform.position = new Vector3(transform.position.x + velocity * Time.fixedDeltaTime,transform.position.y,0);
			velocity = newvelocity;
		}
		if(transform.position.x < min){
			min = transform.position.x;
			//Debug.Log("NEW MIN: " + min);
		}
		if(transform.position.x > max){
			max = transform.position.x;
			//if(newMethod)
			//Debug.Log(newMethod + " NEW MAX: " + max);
		}
	}
}
