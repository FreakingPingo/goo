﻿using UnityEngine;
using System.Collections;

public class PlaneMesh : MonoBehaviour {
	Transform[,] vertPoints;
	Mesh mesh = new Mesh();
	GameObject meshObject;
	public Material mat;

	public void CreateMesh(Transform[,] transforms){
		vertPoints = transforms;

		// Verts positions
		// 0 ------ 1
		// |      /	|
		// |    /	|
		// |  /    	|
		// 2 ------ 3

		/*for(int i = 0; i < transforms.Length; i++){
			vertPoints[i] = transforms[i];
		}*/
		GenerateMesh();
	}

	void Update()
	{
		if(meshObject != null)
			GenerateMesh();
	}

	void GenerateMesh(){
		int[] tris = new int[(vertPoints.GetLength(0) - 1) * (vertPoints.GetLength(1) - 1) * 6];
		Vector3[] verts = new Vector3[vertPoints.GetLength(0) * vertPoints.GetLength(1)];
		Vector2[] uvs = new Vector2[verts.Length];
//		Debug.Log(verts.Length);
		int indexx = 0;
		for(int i = 0; i < vertPoints.GetLength(0); i++)
		{
			for(int j = 0; j < vertPoints.GetLength(1); j++)
			{
				// filling along horizontal axis (x axis) kinda style
//				Debug.Log(vertPoints[i,j]);
				verts[indexx] = vertPoints[i,j].position;
				uvs[indexx] = new Vector2((float)i / (float)vertPoints.GetLength(0), (float)j / (float)vertPoints.GetLength(1));

				indexx++;


			}
		}

		int squareCounter = 0;
		for(int i = 0; i < tris.Length; i++)
		{

			if((squareCounter + 1) % vertPoints.GetLength(1) == 0)
			{
				squareCounter++;
			}
			int index = i % 6;
			switch(index){
			case 0:
//				Debug.Log("Starting square " + squareCounter);
				tris[i] = squareCounter + 1;
				break;
			case 1:
				tris[i] = vertPoints.GetLength(1) + 1 + squareCounter;
				break;
			case 2:
				tris[i] = squareCounter;
				break;
			case 3:
				tris[i] = squareCounter;
				break;
			case 4:
				tris[i] = vertPoints.GetLength(1) + 1 + squareCounter;
				break;
			case 5:
				tris[i] = vertPoints.GetLength(1) + squareCounter;
//				Debug.Log("Done with square " + squareCounter);
				squareCounter++;

				break;
			}

		}



		mesh.vertices = verts;
		mesh.triangles = tris;
		mesh.name = "DatMesh";
		mesh.uv = uvs;
		
		mesh.RecalculateNormals();
		mesh.RecalculateBounds();

		if(meshObject == null){
			meshObject = new GameObject();
			meshObject.transform.parent = this.transform;
			meshObject.AddComponent<MeshFilter>().mesh = mesh;
			meshObject.AddComponent<MeshRenderer>();
			meshObject.GetComponent<MeshRenderer>().material = mat;

		}
		meshObject.GetComponent<MeshRenderer>().material = mat;
		//else
		//	meshObject.GetComponent<MeshFilter>().mesh = mesh;

	}
}
