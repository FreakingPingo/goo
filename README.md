# Demo of deformable bodies in Unity  #

### What is this repository for? ###
As a part of our game physics course, we developed a 2D and 3D demo examples of a deformable body made out of springs components. The idea for this repository is for people who are interested in knowing how this is achieved. The demo have been tested on Unity 4.6 beta version, and should be executable from any 4.x version of Unity.

Feel free to borrow/steal our code as you pleas.

## 2D deformable body ##
![Screenshot 2014-10-01 19.12.14.png](https://bitbucket.org/repo/RM7a4n/images/1912296917-Screenshot%202014-10-01%2019.12.14.png)

## 3D deformable body ##
![Screenshot 2014-10-01 19.11.26.png](https://bitbucket.org/repo/RM7a4n/images/4238134117-Screenshot%202014-10-01%2019.11.26.png)