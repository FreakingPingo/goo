﻿using UnityEngine;
using System.Collections;

public class LineRenderFollow : MonoBehaviour {

	private LineRenderer lineRenderer;
	private Transform target;

	public void Update()
	{
		if(lineRenderer != null)
		{
			lineRenderer.SetPosition(0, transform.position);
			lineRenderer.SetPosition(1, target.position);
		} else
			Debug.LogWarning ("No renderer found");
	}

	public void AttachLineRenderer(Transform to)
	{
		GameObject objLineRenderer = new GameObject("Line renderer");
		objLineRenderer.transform.parent = transform;
		lineRenderer = objLineRenderer.AddComponent<LineRenderer>();
		lineRenderer.SetWidth(0.2f, 0.2f);
		target = to;
	}
}
