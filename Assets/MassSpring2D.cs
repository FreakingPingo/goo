﻿using UnityEngine;
using System.Collections;

public class MassSpring2D : MonoBehaviour {

	public Mesh mesh;
	public Vector3[] vertices;
	public int[] triangles;


	public enum SpringType
	{
		Structure,
		Bending,
		Shear
	}

	private GameObject[,] arrOfRigidbodies;
	public int width = 5;
	public int height = 5;
	public Material ropeMaterial;
	private Rigidbody theChosenOne;
	private float positionX = 0;
	public GameObject node;
	public float springVal = 0;
	public float springDamp = 0;
	public float maxDistance = 0.0f;
	public float minDistance = 0.0f;
	public float breakForce = 0.0f;

	public float totalWeight = 0.3f;
	public float resolution = 1f;
	public bool hookItUp = false;

	public bool enableBendingSprings = true;
	public bool enableShearSprings = true;

	public bool enableSpringColliders = false;
	public bool drawMesh = true;


	void Awake()
	{	
		if(drawMesh)
		{
			mesh = new Mesh();
			mesh.name = "Dynamic Cloth";
			gameObject.AddComponent<MeshFilter>().mesh = mesh;
			MeshRenderer meshRenderer = gameObject.AddComponent<MeshRenderer>();
			meshRenderer.material = ropeMaterial;
		}

		gameObject.name = "Clothing";
		arrOfRigidbodies = new GameObject[width, height];
		positionX = transform.position.x;


		for(int i = 0; i < width; i++)
		{
			for(int j = 0; j < height; j++)
			{
				arrOfRigidbodies[i, j] = CreateNewNode(i, j);
				arrOfRigidbodies[i, j].AddComponent<DragRigidBody>();
				if(i == width * height - 1)
					theChosenOne = arrOfRigidbodies[i,j].rigidbody;

			}
		}

		for(int i = 0; i < arrOfRigidbodies.GetLength(0); i++)
		{
			for(int j = 0; j < arrOfRigidbodies.GetLength(1); j++)
			{
				if(i < arrOfRigidbodies.GetLength(0) - 1)
				{
					if(i + 1 < arrOfRigidbodies.GetLength(0))
					{	
						AddSpring(arrOfRigidbodies[i,j], arrOfRigidbodies[i+1, j], SpringType.Structure); // Addition of structural springs

						if(i + 2 < arrOfRigidbodies.GetLength(0) && enableBendingSprings) 
							AddSpring(arrOfRigidbodies[i,j], arrOfRigidbodies[i + 2, j], SpringType.Bending);	// Addition of bending springs
					}			
				}
				
				if(j < arrOfRigidbodies.GetLength(0) - 1)
				{
					if(j + 1 < arrOfRigidbodies.GetLength(1))
					{
						AddSpring(arrOfRigidbodies[i,j], arrOfRigidbodies[i, j + 1], SpringType.Structure); // Addition of structural springs

						if(j + 2 < arrOfRigidbodies.GetLength(1) && enableBendingSprings)
							AddSpring(arrOfRigidbodies[i,j], arrOfRigidbodies[i, j + 2], SpringType.Bending); // Addition of bending springs

					}
				}


				if(enableShearSprings) // Addition of shearing springs
				{
					if(i != 0 && j != 0)
						AddSpring(arrOfRigidbodies[i, j], arrOfRigidbodies[i - 1, j - 1], SpringType.Shear); // Top left

					if(i != 0 && j != arrOfRigidbodies.GetLength(0) - 1)
						AddSpring (arrOfRigidbodies[i,j], arrOfRigidbodies[i - 1, j + 1], SpringType.Shear);	// Bottom left
				}
			}
		}
	}

	GameObject CreateNewNode(int i, int j)
	{
		GameObject source = (GameObject)Instantiate(node, new Vector3(transform.position.x + i * resolution, transform.position.y ,transform.position.z +  j * resolution), Quaternion.identity);
		source.transform.parent = transform;
		source.name = "("+ i + ", " + j + ")";
		if(source.GetComponent<Rigidbody>() == null)
			source.AddComponent<Rigidbody>();
		source.rigidbody.mass = totalWeight / (width * height);
		source.rigidbody.drag = 0.4f;


		if(((j == 0 && i == 0) || (j == width - 1 && i == 0)) && hookItUp)
			source.rigidbody.isKinematic = true;

		if(!enableSpringColliders)
		{	
			SphereCollider col = source.AddComponent<SphereCollider>();
			col.radius = (float)(resolution / 2 - 0.01f);
		}


		return source;
	}

	void AttachLineRender(Transform from, Transform to)
	{
		from.gameObject.AddComponent<LineRenderFollow>().AttachLineRenderer(to);
	}


	void AddSpring(GameObject source, GameObject to, SpringType type)
	{
		SpringJoint structureSpring = source.AddComponent<SpringJoint>();
		structureSpring.spring = springVal;
		structureSpring.maxDistance = maxDistance;
		structureSpring.minDistance = minDistance;

		structureSpring.damper = springDamp;
		structureSpring.connectedBody = to.rigidbody;
		structureSpring.breakForce = breakForce;

		if(type != SpringType.Bending && !drawMesh)
			AttachLineRender(source.transform, to.transform);

		if(enableSpringColliders && type == SpringType.Structure) //Currently not working
		{	
			GameObject colliderObject = new GameObject("Collider");
			colliderObject.transform.position = source.transform.position;
			colliderObject.transform.parent = source.transform;

			colliderObject.transform.LookAt(to.transform.position);
			colliderObject.transform.position = source.transform.position + ((to.transform.position - source.transform.position) / 2);

			CapsuleCollider col = colliderObject.AddComponent<CapsuleCollider>();
			col.direction = 2;
			col.height = resolution;
		}
	}

	void Update()
	{
		if(drawMesh)
		{
			int counter = 0;
			Vector3[] vertices = new Vector3[width * height];
			Vector3[] normals = new Vector3[vertices.Length];
			Vector2[] uv = new Vector2[vertices.Length];

			int[] triangles = new int[(int)((vertices.Length / 2) * 3 * 4)];
			for(int i = 0; i < arrOfRigidbodies.GetLength(0); i++)
			{
				for(int j = 0; j < arrOfRigidbodies.GetLength(1); j++)
				{
					vertices[counter] = arrOfRigidbodies[i,j].transform.localPosition;
					normals[counter] = Vector3.up;
					uv[counter] = new Vector2((float)i/(float)width, (float)j/(float)height);
					counter++;
				}
			}
				
			mesh.vertices = vertices;
			mesh.normals = normals;

			int triangleCount = 0;
			for(int i = 0; i < vertices.Length - width - 1; i++)
			{
				if(((i + 1) % width != 0))
				{
					triangles[triangleCount] = i;
					triangles[triangleCount + 1] = i + 1;
					triangles[triangleCount + 2] = i + width;

					triangles[triangleCount + 3] = i + 1;
					triangles[triangleCount + 4] = i + width + 1;
					triangles[triangleCount + 5] = i + width;
					triangleCount = triangleCount + 6;
					
				}
			
			}

			mesh.triangles = triangles;
			mesh.uv = uv;
		}
	}
}
